<?php

namespace app\models;

use Yii;
use yii\data\ArrayDataProvider;

class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $email;
    public $roleName;
    public $secretKey;

    public $password;
    public $authKey;
    public $accessToken;

    private static $users = [];

    protected static function saveUsersInSession(){
        $session = Yii::$app->session;
        $session->set('users', self::$users);
        $session->close();
    }
    
    protected static function loadUsersFromSession(){
        $session = Yii::$app->session;
        if($session->has('users')){
            self::$users = $session->get('users');
        }else{
            self::$users = [];
        }
        $session->close();
    }

    public function getUserDataProvider(){
        $usedParams = ['id', 'username', 'email', 'roleName', 'secretKey'];
        $data = [];
        foreach ($usedParams as $paramName) {
            array_push(
                $data, 
                [
                    'title' => $paramName,
                    'value' => ($paramName == 'secretKey') ? base64_decode($this->{ $paramName }) : $this->{ $paramName }
                ]
            );
        }

        return new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        self::loadUsersFromSession();
        foreach (self::$users as $user) {
            if ($user['id'] == $id) {
                return new static($user);
            }
        }
        
        return null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        self::loadUsersFromSession();
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }
        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        self::loadUsersFromSession();
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    public static function createByData($data)
    {   
        array_push(self::$users, [
            'id' => $data->id,
            'username' => $data->name,
            'email' => $data->email,
            'roleName' => $data->role->name,
            'secretKey' => $data->secretKey,
        ]);

        self::saveUsersInSession();
        
        return new static(end(self::$users));
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return true;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return true;
    }
}
