<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\helpers\CurlHelper;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $company;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email', 'password', 'company'], 'required'],
            ['email', 'email'],
        ];
    }

    /**
     * Logs in a user using the provided username, password and company.
     * @return bool whether the user is logged in successfully
     */
    public function authorize()
    {        
        $authUrl = "https://api.shamandev.com/auth/login";

        $data = [
            'email' => $this->email,
            'password' => $this->password,
            'curlname' => $this->company
        ];
        // Получаем токен для авторизации на API
        $response = CurlHelper::sendPost($authUrl, $data);        

        if($response['status'] == 200){
            $result = json_decode($response['html']);
            if ( json_last_error() === JSON_ERROR_NONE ) {
                if(isset($result->token)){
                    // авторизуем пользователя на сайте
                    $user = $this->getUser($result->token);
                    if($user){
                        return Yii::$app->user->login($user, 0);    
                    }                                                       
                }else{
                    $this->addError('', 'API error: Response not have token.');
                }
            }else{
                 $this->addError('', 'API error: Response format is not JSON.');
            }
        }else{
           $this->addError('', 'API error: ' . $response['html']);
        }
        return false;
    }

    /**
     * Finds or create user by [[token]]
     *
     * @return User|null
     */
    public function getUser($token)
    {
        if ($this->_user === false) {
            // Получаем данные пользователя
            $userUrl = "https://api.shamandev.com/user/current";
            $response = CurlHelper::sendGet($userUrl, $headers = 'Authorization: Bearer ' . $token . PHP_EOL );

            if($response['status'] == 200){
                $result = json_decode($response['html']);
                if ( json_last_error() === JSON_ERROR_NONE ) {
                    $this->_user = User::createByData($result);
                }else{
                    $this->addError('', 'API error: Response format is not JSON.');
                }
            }else{
                $this->addError('', 'API error: ' . $response['html']);
            }   
        }
        return $this->_user;
    }
}
