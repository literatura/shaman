<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Shaman API';
?>
<div class="site-index">
    <div class="body-content">
        <div class="page-header">
            <h1>User: <?= Html::encode(Yii::$app->user->identity->username) ?></h1>
        </div>
        <div class="row">            
            <div class="col-xs-12 col-md-12">
                <?= GridView::widget([
                    'dataProvider' => $userData,
                    'columns' => [
                        'title',
                        'value',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
