<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Shaman API';
?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-xs-12 col-md-6 col-md-offset-4">
                <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-lg-6\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                        'labelOptions' => ['class' => 'col-lg-3 control-label'],
                    ],
                ]); ?>

                    <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                    <?= $form->field($model, 'password')->passwordInput() ?>

                    <?= $form->field($model, 'company')->textInput() ?>

                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-9">
                            <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                            <?= Html::button('Demo', ['class' => 'btn btn-secondary default-button', 'name' => 'default-button']) ?>
                        </div>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<?php
// Код для заполнения полей формы логина демо-данными
$script = <<< JS
    $('.default-button').on('click',function(){
        $('#loginform-email').val('test@user.demo');
        $('#loginform-password').val('1234567A');
        $('#loginform-company').val('web-2015');
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);

?>
