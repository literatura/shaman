<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Shaman API';

?>
<div class="site-about">
    <div class="jumbotron">
        <h1><?= Html::encode($this->title) ?></h1>
        <p>
            Welcome to Shaman API.
        </p>
        <p>
            <?= Html::a('Login', ['/site/login'], ['class' => 'btn btn-primary btn-lg']) ?>
        </p>
    </div>   
</div>
