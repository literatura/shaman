<?php
namespace app\helpers;

/**
* Хелпер для отправки запросов POST и GET
* исплользуется curl или file_get_contents если curl не доступен
*/


class CurlHelper
{
    public static function sendPost($url, $data){
        if (function_exists('curl_init')){
            return self::curlRequest($url, $data);
        }else{
            return self::contentsRequest($url, $data);
        }
    }

    public static function sendGet($url, $headers = ""){
        if (function_exists('curl_init')){
           return self::curlRequest($url, $data = [], $post = false, true, $headers );
        }else{
            return self::contentsRequest($url, $data = [], $post = false, $headers);
        }
    }

    /* Отправка запросов с использованием curl */
    protected static function curlRequest($url, $post_params = [], $post = true, $http_build_query = true, $headers = "")
    {        
        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 15);
        curl_setopt($c, CURLOPT_TIMEOUT, 120);
        
        // Т.к. curl принимает заголовки в виде массива, преобразуем строку в массив
        if(!empty($headers)){
            $headersArr = explode(PHP_EOL, $headers);
            if(empty(end($headersArr))){
                array_pop($headersArr);
            }
            curl_setopt($c, CURLOPT_HTTPHEADER, $headersArr );
        }

        if ($post) {
            curl_setopt($c, CURLOPT_POST, TRUE);
            if($http_build_query){
                $query = http_build_query($post_params);
                curl_setopt($c, CURLOPT_POSTFIELDS, $query);
            }else{
                curl_setopt($c, CURLOPT_POSTFIELDS, $post_params);
            }   
        }        

        $response = curl_exec($c);
        if(!$response){
            $response = curl_error($c);
        }
        $httpStatus = curl_getinfo($c);
        curl_close($c);

        if($httpStatus['http_code'] != 200){
            // пробуем распарсить ответ как json
            $res = json_decode($response, true);
            if ( json_last_error() === JSON_ERROR_NONE ) { //Если удалось, то формируем сообщение об ошибке
                $status = $res['status'];
                $html = $res['name'];
                if(isset($res['message']) && !empty($res['message'])){
                    if(is_array($res['message'])){
                        $html .= ": ";
                        foreach ($res['message'] as $msg) {
                            $html .= $msg . PHP_EOL;
                        }
                    }else{
                        $html .= ": ".$res['message'];
                    }                    
                }
            }else{ // если ответ не json, то что-то пошло не так
                $status = 500; // TODO: какой лучше код и сообщение возвращать?
                $html = $response;
            }             
        }else{
            $status = $httpStatus['http_code'];
            $html = $response;
        }

        $result =[
            'status' => $status, 
            'html' => $html
        ];
        return $result;
    }

    /* Отправка запросов с использованием file_get_contents */
    protected static function contentsRequest($url, $post_params = [], $post = true, $headers = ""){
        $httpParams = [
            'method' => $post ? 'POST' : 'GET',
            'ignore_errors' => true,
            'header' => $headers
        ];
        if($post){
            $httpParams['header'] .= 'Content-Type: application/x-www-form-urlencoded' . PHP_EOL;
            $httpParams['content'] = http_build_query($post_params);
        }

        $context = stream_context_create([
            'http' => $httpParams
        ]);

        $response = file_get_contents($url, $use_include_path = false, $context);

        if(strpos($http_response_header[0], "200")){ // Если запрос удачен
            $status = 200;
            $html = $response;
        }else{ // если запрос вернул ошибку
            // пробуем распарсить ответ как json
            $res = json_decode($response, true);
            if ( json_last_error() === JSON_ERROR_NONE ) { //Если удалось, то формируем сообщение об ошибке
                $status = $res['status'];
                $html = $res['name'];
                if(isset($res['message']) && !empty($res['message'])){
                    if(is_array($res['message'])){
                        $html .= ": ";
                        foreach ($res['message'] as $msg) {
                            $html .= $msg . PHP_EOL;
                        }
                    }else{
                        $html .= ": ".$res['message'];
                    }
                    
                }
            }else{ // если ответ не json, то что-то пошло не так
                $status = 500; // TODO: какой лучше код и сообщение возвращать?
                $html = $response;
            }             
        }

        $result = [
            'status' => $status, 
            'html' => $html
        ];
        return $result;
    }
}


